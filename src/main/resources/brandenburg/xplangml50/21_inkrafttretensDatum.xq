declare default element namespace 'http://www.xplanung.de/xplangml/5/0';
declare namespace gml='http://www.opengis.net/gml/3.2';
declare namespace xlink='http://www.w3.org/1999/xlink';

for $h in //BP_Plan
where (
    not($h/inkrafttretensDatum/text())
    and 
    $h/rechtsstand = ('4000','4500')
)
return $h/@gml:id/string()
