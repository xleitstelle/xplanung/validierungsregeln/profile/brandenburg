declare default element namespace 'http://www.xplanung.de/xplangml/5/4';
declare namespace gml='http://www.opengis.net/gml/3.2';
declare namespace xlink='http://www.w3.org/1999/xlink';

for $h in //BP_GewaesserFlaeche
where not(
    $h/zweckbestimmung/text()
)
return $h/@gml:id/string()


