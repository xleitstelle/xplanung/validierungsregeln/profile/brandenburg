# Umsetzung des Brandenburger Profils #

## Inhalt des Repositories

Dieses Repository beinhaltet XPlanung-Validierungsregeln, die den [XPlanung-Standard](https://xleitstelle.de/xplanung/releases-xplanung) um zusätzliche Validierungsregeln für das Land Brandenburg erweitern. Diese Validierungsregeln sind als [XQuery](https://www.w3.org/XML/Query/)-Anweisungen umgesetzt.

## Autoren

* [XLeitstelle Planen und Bauen](https://xleitstelle.de)

Personen, die an diesem Projekt mitgearbeitet haben, stehen in [CONTRIBUTORS](CONTRIBUTORS.md).

## Lizenz

Dieses Projekt ist unter der Open-Source-Lizenz für die Europäische Union („EUPL“) v1.2 veröffentlicht. Weiter Informationen zur Lizenz stehen in [LICENSE.txt](LICENSE.txt). 

### Umfang des Brandenburger Profils ###

Der Umfang dieses Validierungsprofils ist durch das [Pflichtenheft des LBV Brandenburg](https://lbv.brandenburg.de/download/Raumbeobachtung/Pflichtenheft_2022.pdf) (S. 24 ff.) definiert.
